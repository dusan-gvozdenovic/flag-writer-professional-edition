"\033[1mEnd-User License Agreement (EULA) of Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9\033[0m\n\n"
"This End-User License Agreement (\"EULA\") is a legal agreement between you and DESCON.ME\n\n"
"This EULA agreement governs your acquisition and use of our Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9 software (\"Software\") directly from DESCON.ME or indirectly through a DESCON.ME authorized reseller or distributor (a \"Reseller\").\n\n"
"Please read this EULA agreement carefully before completing the installation process and using the Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9 software. It provides a license to use the Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9 software and contains warranty information and liability disclaimers.\n\n"
"If you register for a free trial of the Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9 software, this EULA agreement will also govern that trial. By clicking \"accept\" or installing and/or using the Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9 software, you are confirming your acceptance of the Software and agreeing to become bound by the terms of this EULA agreement.\n\n"
"If you are entering into this EULA agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity and its affiliates to these terms and conditions. If you do not have such authority or if you do not agree with the terms and conditions of this EULA agreement, do not install or use the Software, and you must not accept this EULA agreement.\n\n"
"This EULA agreement shall apply only to the Software supplied by DESCON.ME herewith regardless of whether other software is referred to or described herein. The terms also apply to any DESCON.ME updates, supplements, Internet-based services, and support services for the Software, unless other terms accompany those items on delivery. If so, those terms apply. This EULA was created by EULA Template for Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9.\n\n"
"\033[1mLicense Grant\033[0m\n\n"
"DESCON.ME hereby grants you a personal, non-transferable, non-exclusive licence to use the Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9 software on your devices in accordance with the terms of this EULA agreement.\n\n"
"You are permitted to load the Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9 software (for example a PC, laptop, mobile or tablet) under your control. You are responsible for ensuring your device meets the minimum requirements of the Flag Generator: Professional Edition\xe2\x84\xa2 \xc2\xa9 software.\n\n"
"You are not permitted to:\n\n"
"\xe2\x80\xa2 Edit, alter, modify, adapt, translate or otherwise change the whole or any part of the Software\n"
"  nor permit the whole or any part of the Software to be combined with or become incorporated in any other software,\n"
"  nor decompile, disassemble or reverse engineer the Software or attempt to do any such things\n"
"\xe2\x80\xa2 Reproduce, copy, distribute, resell or otherwise use the Software for any commercial purpose\n"
"\xe2\x80\xa2 Allow any third party to use the Software on behalf of or for the benefit of any third party\n"
"\xe2\x80\xa2 Use the Software in any way which breaches any applicable local, national or international law\n"
"\xe2\x80\xa2 use the Software for any purpose that DESCON.ME considers is a breach of this EULA agreement\n\n"

"\033[1mIntellectual Property and Ownership\033[0m\n\n"

"DESCON.ME shall at all times retain ownership of the Software as originally downloaded by you and all subsequent downloads of the Software by you. The Software (and the copyright, and other intellectual property rights of whatever nature in the Software, including any modifications made thereto) are and shall remain the property of DESCON.ME.\n\n"

"DESCON.ME reserves the right to grant licences to use the Software to third parties.\n\n"

"\033[1mTermination\033[0m\n\n"

"This EULA agreement is effective from the date you first use the Software and shall continue until terminated. You may terminate it at any time upon written notice to DESCON.ME.\n\n"

"It will also terminate immediately if you fail to comply with any term of this EULA agreement. Upon such termination, the licenses granted by this EULA agreement will immediately terminate and you agree to stop all access and use of the Software. The provisions that by their nature continue and survive will survive any termination of this EULA agreement.\n\n"

"\033[1mGoverning Law\033[0m\n\n"

"This EULA agreement, and any dispute arising out of or in connection with this EULA agreement, shall be governed by and construed in accordance with the laws of rs.\n\n"
