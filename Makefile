CC=gcc
PROGRAM=flag-generator
BUILD=build

all:
	$(CC) main.c -o $(BUILD)/$(PROGRAM)-linux-x86_64 -static -O3 -g
