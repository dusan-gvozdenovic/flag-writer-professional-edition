#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>

#define KEY "CLXRDA-DUY5F2-5QL7ZW"

unsigned volatile valid_hash = 2438613;

static inline unsigned powerzz(unsigned x, unsigned y) {
	unsigned sum = 1;
	while (y--) { sum *= x; }
	return sum;
}

unsigned hash(char *str, size_t length) {
	unsigned sum = 0;
	for (int i = 0; i < length; i++) {
		sum += str[i] * (unsigned) powerzz(31, i) % (1 << (19 - 1));
	}
	return sum;
}

int main() {
	printf("\033[2J%s\n",
#include "eula.h"
	);

	printf("%s", "Accept \033[1m[ENTER]\033[0m / Ctrl+C \033[1m[DECLINE]\033[0m");

	while (getc(stdin) != '\n');

	printf("%s", "\033[1;1H\033[2J");

	printf("%s", "Enter your serial key below to activate software\n\n");

	printf("\e[1m%s\e[0m", "Serial Key: ");

	char key[21];

	fgets(key, 21, stdin);

	printf("\n");

	if (hash(key, strnlen(key, 20)) != valid_hash) {
		printf("\033[0;31mERROR:\033[0m Invalid serial key. Please try again or contact our technical support.\n");
		exit(0);
	}

	int arr[] = {
#include "junk.h"
		, 14, 6, 13, 17, 2, 4, 11, 8, 1, 19, 9, 0, 15, 3, 18, 20, 5, 12, 7, 16, 10,
#include "junk2.h"
	};

	int arr2[] = { 36, 116, 123, 95, 100, 33, 67, 82, 110, 117, 125, 48, 100, 84, 68, 51, 97, 70, 57, 95, 72 };

	int *not_junk = arr + 713;

	printf("\033[1;32m");

	for (int i = 0; i < 21; i++) { putc(arr2[not_junk[i]], stdout); }
	
	printf("\033[0m\n");

	return 0;
}
